import csv
from collections import defaultdict

import packaging.requirements


totals = defaultdict(int)

with open('specifiers.csv') as f:
    reader = csv.reader(f)

    with open('out.csv', 'w') as o:
        writer = csv.writer(o)
        for row in reader:
            req, count = row
            try:
                rrr = packaging.requirements.Requirement(req)

                specs = rrr.specifier._specs
                if 'LegacySpecifier' in [type(x).__name__ for x in specs]:
                    totals['LegacySpecifier'] += int(count)
                else:
                    totals['Specifier'] += int(count)
            except packaging.requirements.InvalidRequirement:
                totals['invalid'] += int(count)

print(totals)