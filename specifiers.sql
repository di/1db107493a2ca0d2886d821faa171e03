copy (
  select 
    specifier, 
    count(specifier) 
  from 
    release_dependencies 
  group by 
    specifier
) to STDOUT CSV;